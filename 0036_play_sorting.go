// https://gobyexample.com/sorting

package main

import (
	"fmt"
	"sort"
)

func main() {
	strings := []string{"b", "c", "a"}
	sort.Strings(strings)
	fmt.Println(strings)

	ints := []int{5, 2, 4, 7, 1}
	sort.Ints(ints)
	fmt.Println(ints)

	ok := sort.IntsAreSorted(ints)
	if ok {
		fmt.Println("ints are sorted")
	}
}
