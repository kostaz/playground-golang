// https://systembash.com/a-simple-go-tcp-server-and-tcp-client/

package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {
	// connect to this socket
	conn, _ := net.Dial("tcp", "127.0.0.1:8081")

	for {
		// read in input from stdin
		msgTx, _ := bufio.NewReader(os.Stdin).ReadString('\n')

		// send to socket
		fmt.Fprintf(conn, msgTx)
		fmt.Printf("Client: Sent message: %s", msgTx)

		// listen for reply
		msgRx, _ := bufio.NewReader(conn).ReadString('\n')
		fmt.Println("Client: Recv message:", msgRx)
	}
}
