// https://gobyexample.com/timers

// We often want to execute Go code at some point in the future, or
// repeatedly at some interval. Go’s built-in timer and ticker features
// make both of these tasks easy. We’ll look first at timers and then at
// tickers.

package main

import (
	"fmt"
	"time"
)

func main() {
	// Timer starts downcount RIGHT after creation!
	timer1 := time.NewTimer(time.Second * 2)
	<-timer1.C
	fmt.Println("Timer 1 expired")

	// time.Sleep(..) can not be stopped.
	// But timer can be stopped before it expires.
	timer2 := time.NewTimer(time.Second * 1)
	go func() {
		<-timer2.C
		fmt.Println("Timer 2 expired")
	}()

	stop2 := timer2.Stop()
	if stop2 {
		fmt.Println("Timer 2 stopped")
	}
}
