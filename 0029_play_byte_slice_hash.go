package main

import (
	"fmt"
	"math/rand"
)

func main() {
	bs := make([]byte, 32)

	for i := 0; i < len(bs); i++ {
		bs[i] = byte(i)
	}

	// Do not allocate new slices.
	// Slice the existing slice.
	for i := 0; i < len(bs); i++ {
		s := bs[i : i+1]
		rand.Read(s)
		bs[i] = s[0]
	}

	fmt.Printf("0x%x\n", bs)
}
