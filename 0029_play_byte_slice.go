// As part of Bitcoin protocol, we calculate hash of various
// data. Hence, let's give a tiny snapshot of how to do it.

package main

import (
	"crypto/sha256"
	"fmt"
	"math/rand"
)

func main() {
	var bs []byte

	// Create 32-byte random data.
	for i := 0; i < 32; i++ {
		rb := make([]byte, 1)
		rand.Read(rb)
		bs = append(bs, rb...) // convert slice to dots
	}

	// Calculate data hash.
	h := sha256.New()
	h.Write(bs)
	bh := h.Sum(nil)

	fmt.Printf("data 0x%x\n", bs)
	fmt.Printf("hash 0x%x\n", bh)
}
