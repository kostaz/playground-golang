Playground for Go language! :-)

Gotchas
=======

- `:GoDef` - Jumps to definition, also for go standard libraries.
             For example, `bufio.NewScanner().Scan()`.
