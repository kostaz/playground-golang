// https://rosettacode.org/wiki/Bitcoin/address_validation#Go

package main

import (
	"bytes"
	"crypto/sha256"
	"errors"
	"fmt"
	"os"
)

type A25 [25]byte

func (a *A25) Version() byte {
	return a[0]
}

func (a *A25) EmbdCS() (c [4]byte) {
	copy(c[:], a[21:])
	return
}

func (a *A25) CalcCS() (c [4]byte) {
	copy(c[:], a.doubleSha256())
	return
}

func (a *A25) doubleSha256() []byte {
	h := sha256.New()
	h.Write(a[:21])
	d := h.Sum(nil)

	h = sha256.New()
	h.Write(d)
	return h.Sum(nil)
}

var tmpl = []byte("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz")

func (a *A25) Set58(s []byte) error {
	for _, s1 := range s {
		c := bytes.IndexByte(tmpl, s1)
		if c < 0 {
			return errors.New("bad char")
		}
		for j := 24; j >= 0; j-- {
			c += 58 * int(a[j])
			a[j] = byte(c % 256)
			c /= 256
		}
		if c > 0 {
			return errors.New("too long")
		}
	}
	return nil
}

func (a *A25) isValidAddr() error {
	if a.Version() != 0x00 {
		return errors.New("wrong version")
	}
	if a.EmbdCS() != a.CalcCS() {
		return errors.New("bad CS")
	}
	return nil
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Error: no bitcoin address")
		return
	}

	s58 := os.Args[1]

	fmt.Printf("Base58 address: %s\n", s58)

	var a A25
	if e := a.Set58([]byte(s58)); e != nil {
		fmt.Println(e)
		return
	}

	fmt.Printf("log: Base58 address  : [%s], (%02d bytes)\n", s58, len(s58))
	fmt.Printf("log: Base16 address  : [%x], (%02d bytes)\n", a, len(a))
	fmt.Printf("log: Address version : [%02x]\n", a.Version())
	fmt.Printf("log: Embedded CS     : [%02x]\n", a.EmbdCS())
	fmt.Printf("log: Calculated CS   : [%02x]\n", a.CalcCS())

	if e := a.isValidAddr(); e != nil {
		fmt.Println(e)
		return
	}
}
