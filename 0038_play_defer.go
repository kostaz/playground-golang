// https://gobyexample.com/defer

package main

import (
	"fmt"
	"os"
)

func createFile() *os.File {
	fmt.Println("Creating")
	f, e := os.Create("./tmp.txt")
	if e != nil {
		panic(e)
	}
	return f
}

func writeFile(f *os.File) {
	fmt.Println("Writing")
	fmt.Fprintf(f, "\nddd\n")
}

func closeFile(f *os.File) {
	fmt.Println("Closing")
	f.Close()
}

func main() {
	fmt.Println(os.Args[0])
	f := createFile()
	defer closeFile(f)
	writeFile(f)
}
