// https://gobyexample.com/stateful-goroutines

package main

import (
	"fmt"
	"math/rand"
	"sync/atomic"
	"time"
)

type readOp struct {
	key int
	val chan int
}

type writeOp struct {
	key  int
	val  int
	done chan bool
}

func main() {
	state := make(map[int]int)
	reads := make(chan *readOp)
	writes := make(chan *writeOp)
	var readOps uint64 = 0
	var sumRead uint64 = 0
	var writeOps uint64 = 0
	var sumWrite uint64 = 0

	go func() {
		for {
			select {
			case rd := <-reads:
				rd.val <- state[rd.key]
			case wr := <-writes:
				state[wr.key] = wr.val
				wr.done <- true
			}
		}
	}()

	// Without creating `make(chan int)` execution is stuck!
	for i := 0; i < 100; i++ {
		go func() {
			for {
				read := &readOp{
					key: rand.Intn(5),
					val: make(chan int), // must-have!
				}
				reads <- read

				atomic.AddUint64(&sumRead, uint64(<-read.val))
				time.Sleep(time.Millisecond)
				atomic.AddUint64(&readOps, 1)
			}
		}()
	}

	// Without creating `make(chan bool)` execution is stuck!
	for i := 0; i < 100; i++ {
		go func() {
			for {
				write := &writeOp{
					key:  rand.Intn(5),
					val:  rand.Intn(100),
					done: make(chan bool), // must-have!
				}
				writes <- write
				<-write.done

				atomic.AddUint64(&sumWrite, uint64(write.val))
				time.Sleep(time.Millisecond)
				atomic.AddUint64(&writeOps, 1)
			}
		}()
	}

	time.Sleep(time.Second)

	fmt.Println(atomic.LoadUint64(&readOps))
	fmt.Println(atomic.LoadUint64(&writeOps))
	fmt.Println(atomic.LoadUint64(&sumRead))
	fmt.Println(atomic.LoadUint64(&sumWrite))
	fmt.Println(state)
}
