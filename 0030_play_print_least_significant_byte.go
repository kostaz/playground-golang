package main

import (
	"fmt"
	"math/rand"
)

// Generate pseudo-random 32-byte data
func getData() []byte {
	bs := make([]byte, 32)
	randByte := make([]byte, 1)

	for i := 0; i < 32; i++ {
		rand.Read(randByte)
		bs[i] = randByte[0]
	}

	return bs
}

func main() {
	bytes := getData()

	// Print MSB and LSB bytes of 32-byte data.
	fmt.Printf("All bytes 0x%x\n", bytes)
	fmt.Printf("MSB byte  0x%x\n", bytes[0])
	fmt.Printf("LSB byte  0x%x\n", bytes[len(bytes)-1])
}
