// https://gobyexample.com/non-blocking-channel-operations

// Basic sends and receives on channels are blocking. However, we can use
// select with a default clause to implement non-blocking sends,
// receives, and even non-blocking multi-way selects.

package main

import "fmt"

func main() {
	messages := make(chan string)

	// Chan receive is stuck, hence, fall to default.
	select {
	case msg := <-messages:
		fmt.Println("received msg:", msg)
	default:
		fmt.Println("no message received")
	}

	// No chan receive, hence, send to chan is stuck, hence, default.
	msg := "hi"
	select {
	case messages <- msg:
		fmt.Println("sent message", msg)
	default:
		fmt.Println("no message sent")
	}

	signals := make(chan bool)
	select {
	case msg := <-messages:
		fmt.Println("received message", msg)
	case sig := <-signals:
		fmt.Println("received signal", sig)
	default:
		fmt.Println("no activity")
	}
}
