package main

import "fmt"

func main() {
	var b uint8 = 0xce
	fmt.Printf("%T\n", b)
	fmt.Printf("%v\n", b)
	fmt.Printf("0x%x\n", b)
	fmt.Printf("%b\n", b)
	fmt.Printf("LSB %d\n", b&0x01)
	fmt.Printf("MSB %d\n", b&0x80>>7)
}
