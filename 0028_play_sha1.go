// https://gobyexample.com/sha1-hashes

package main

import (
	"crypto/sha1"
	"fmt"
)

func main() {
	s := "datatohash"
	h := sha1.New()

	h.Write([]byte(s))
	bs := h.Sum(nil)

	fmt.Println(s)
	fmt.Printf("0x%x\n", bs)
}
