// https://gobyexample.com/sorting-by-functions

package main

import (
	"fmt"
	"sort"
)

type ByLen []string

func (b ByLen) Len() int {
	return len(b)
}

func (b ByLen) Swap(i, j int) {
	b[i], b[j] = b[j], b[i]
}

func (b ByLen) Less(i, j int) bool {
	return len(b[i]) < len(b[j])
}

func main() {
	strings := []string{"aa", "bbb", "aaaa"}
	sort.Sort(ByLen(strings))
	fmt.Println(strings)
}
