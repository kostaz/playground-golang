// https://gobyexample.com/worker-pools

// Implement a worker pool using goroutines and channels.

package main

import (
	"fmt"
	"time"
)

func worker(id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		fmt.Println("worker", id, "started job", j)
		time.Sleep(time.Second * 1)
		fmt.Println("worker", id, "started job", j)
		results <- j * 2
	}
	fmt.Println("worker", id, "no more jobs")
}

func main() {
	jobs := make(chan int, 100)
	results := make(chan int, 100)

	for i := 0; i < 3; i++ {
		go worker(i, jobs, results)
	}

	for j := 0; j < 5; j++ {
		jobs <- j
	}

	close(jobs)

	for i := 0; i < 5; i++ {
		fmt.Println("result", <-results)
	}

	var done string
	fmt.Scanln(&done)
}
