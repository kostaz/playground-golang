// https://gobyexample.com/channel-synchronization

// We can use channels to synchronize execution across goroutines. Here’s
// an example of using a blocking receive to wait for a goroutine to
// finish.

package main

import (
	"fmt"
	"time"
)

func worker(done chan bool) {
	fmt.Print("working...")
	time.Sleep(time.Second)
	fmt.Println("done")
	done <- true
}

func main() {
	// Synchronous version.
	done := make(chan bool, 1)
	worker(done)
	<-done

	// Asynchronous version.
	done_async := make(chan bool)
	go worker(done_async)
	<-done_async
}
