// https://gobyexample.com/mutexes

package main

import (
	"fmt"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	state := make(map[int]int)

	var readOps uint64 = 0
	var writeOps uint64 = 0

	var mutex = &sync.Mutex{}

	for i := 0; i < 100; i++ {
		go func() {
			total := 0
			for {
				key := rand.Intn(5)
				mutex.Lock()
				total += state[key]
				mutex.Unlock()

				atomic.AddUint64(&readOps, 1)
				time.Sleep(time.Millisecond)
			}
		}()
	}

	for i := 0; i < 100; i++ {
		go func() {
			for {
				key := rand.Intn(5)
				val := rand.Intn(100)
				mutex.Lock()
				state[key] = val
				mutex.Unlock()

				atomic.AddUint64(&writeOps, 1)
				time.Sleep(time.Millisecond)
			}
		}()
	}

	time.Sleep(time.Second)

	fmt.Println("Read ops:", atomic.LoadUint64(&readOps))
	fmt.Println("Write ops:", atomic.LoadUint64(&writeOps))

	mutex.Lock()
	fmt.Println(state)
	mutex.Unlock()
}
