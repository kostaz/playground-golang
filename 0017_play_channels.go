// https://gobyexample.com/channels

// Channels are the pipes that connect concurrent goroutines. You can
// send values into channels from one goroutine and receive those values
// into another goroutine.

package main

import "fmt"

func main() {
	messages := make(chan string)

	// The below function MUST be deferred.
	// Otherwise `main` is stuck (no corresponding receive).
	go func() {
		messages <- "ping"
	}()

	msg := <-messages
	fmt.Println(msg)
}
