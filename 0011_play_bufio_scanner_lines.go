// https://golang.org/pkg/bufio/#example_Scanner_lines

// Scanner provides a convenient interface for reading data such as a
// file of newline-delimited lines of text. Successive calls to the Scan
// method will step through the 'tokens' of a file, skipping the bytes
// between the tokens. The specification of a token is defined by a split
// function of type SplitFunc; the default split function breaks the
// input into lines with line termination stripped. Split functions are
// defined in this package for scanning a file into lines, bytes,
// UTF-8-encoded runes, and space-delimited words. The client may instead
// provide a custom split function.
//
// Scanning stops unrecoverably at EOF, the first I/O error, or a token
// too large to fit in the buffer. When a scan stops, the reader may have
// advanced arbitrarily far past the last token. Programs that need more
// control over error handling or large tokens, or must run sequential
// scans on a reader, should use bufio.Reader instead.

package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Got error:", err)
	}
}
