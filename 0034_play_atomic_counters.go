// https://gobyexample.com/atomic-counters

package main

import (
	"fmt"
	"sync/atomic"
	"time"
)

func main() {
	var counter uint64

	for i := 0; i < 100; i++ {
		go func() {
			for {
				atomic.AddUint64(&counter, 1)
				time.Sleep(time.Millisecond)
			}
		}()
	}

	time.Sleep(time.Second)

	fmt.Printf("%v\n", atomic.LoadUint64(&counter))
	fmt.Printf("%v\n", atomic.LoadUint64(&counter))

	fmt.Printf("%T\n", counter)
	fmt.Printf("%u\n", counter)
}
