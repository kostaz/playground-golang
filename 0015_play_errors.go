// https://gobyexample.com/errors

// In Go it’s idiomatic to communicate errors via an explicit, separate
// return value. This contrasts with the exceptions used in languages
// like Java and Ruby and the overloaded single result / error value
// sometimes used in C. Go’s approach makes it easy to see which
// functions return errors and to handle them using the same language
// constructs employed for any other, non-error tasks.

package main

import (
	"errors"
	"fmt"
)

// Example for built-in error errors.New()
func foo1(arg int) (int, error) {
	if arg == 42 {
		return -1, errors.New("Errrr 42 :-0")
	}

	return arg + 33, nil
}

// It’s possible to use custom types as errors by implementing the
// Error() method on them. Here’s a variant on the example above that
// uses a custom type to explicitly represent an argument error.
type argError struct {
	arg  int
	prob string
}

func (e *argError) Error() string {
	return fmt.Sprintf("%d - %s", e.arg, e.prob)
}

func foo2(arg int) (int, error) {
	if arg == 42 {
		return -1, &argError{arg, "bad num"}
	}
	return arg + 33, nil
}

func main() {
	// Run with simple error handling.
	for _, k := range []int{11, 42} {
		if n, e := foo1(k); e != nil {
			fmt.Println(e)
		} else {
			fmt.Println(n)
		}
	}

	// Run with custom error handling.
	for _, k := range []int{11, 42} {
		if n, e := foo2(k); e != nil {
			fmt.Println(e)
		} else {
			fmt.Println(n)
		}
	}

	// If you want to programmatically use the data in a custom
	// error, you’ll need to get the error as an instance of the
	// custom error type via type assertion.
	_, e := foo2(42)
	if ae, ok := e.(*argError); ok {
		fmt.Println(ae.arg)
		fmt.Println(ae.prob)
	}
}
