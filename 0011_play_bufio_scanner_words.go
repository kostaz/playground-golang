// https://golang.org/pkg/bufio/#example_Scanner_lines

// Use a Scanner to implement a simple word-count utility by scanning the input as
// a sequence of space-delimited tokens.

package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	// An artificial input source.
	// const input = "Now is the winter of our discontent,\nMade glorious summer by this sun of York.\n"
	// scanner := bufio.NewScanner(strings.NewReader(input))

	// If using os.Stdin, but then press Enter and Ctrl-d when finished.
	scanner := bufio.NewScanner(os.Stdin)

	// Set the split function for the scanning operation.
	scanner.Split(bufio.ScanWords)

	// Count the words.
	count := 0
	for scanner.Scan() {
		count++
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Got err:", err)
	}
	fmt.Println("Count:", count)
}
