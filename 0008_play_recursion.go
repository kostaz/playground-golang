// https://gobyexample.com/recursion
// Go supports recursive functions. Here’s a classic factorial example.

package main

import "fmt"

func fact(n int) int {
	if n == 0 {
		return 1
	}
	return n * fact(n-1)
}

func main() {
	for n := 0; n < 17; n++ {
		fmt.Println(n, ":", fact(n))
	}
}

// 0 : 1
// 1 : 1
// 2 : 2
// 3 : 6
// 4 : 24
// 5 : 120
// 6 : 720
// 7 : 5040
// 8 : 40320
// 9 : 362880
// 10 : 3628800
// 11 : 39916800
// 12 : 479001600
// 13 : 6227020800
// 14 : 87178291200
// 15 : 1307674368000
// 16 : 20922789888000
