// https://gobyexample.com/arrays

package main

import "fmt"

func main() {
	fmt.Println("Hi arrays!")

	// declare array of 5 integers
	var a [5]int
	fmt.Println("Empty array of five ints:", a)
	fmt.Println("Array len:", len(a))

	// declare and initialize an array in one line
	b := [5]int{1, 2, 3, 4, 5}
	fmt.Println("Decl and init right away:", b)

	// declare two-dimensional array of
	// integers of 2 rows and 3 columns
	var twoD [2][3]int
	for i := 0; i < 2; i++ {
		for j := 0; j < 3; j++ {
			twoD[i][j] = i + j
		}
	}
	// printed as:
	// twoD: [[0 1 2] [1 2 3]]
	fmt.Println("twoD:", twoD)
}
