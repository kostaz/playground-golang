// https://systembash.com/a-simple-go-tcp-server-and-tcp-client/

package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

func main() {
	fmt.Println("Launching server...")

	// listen on all interfaces
	ln, _ := net.Listen("tcp", ":8081")

	// accept connection on port
	conn, _ := ln.Accept()

	// run loop forever (or until ctrl-c)
	for {
		// will listen for message to process ending in newline (\n)
		msg, _ := bufio.NewReader(conn).ReadString('\n')

		// output received message
		fmt.Printf("Server: Recv message: %s", string(msg))

		// process the message
		newMsg := strings.ToUpper(msg)

		// send new string back to client
		conn.Write([]byte(newMsg + "\n"))
		fmt.Printf("Server: Sent message: %s", newMsg)
	}
}
