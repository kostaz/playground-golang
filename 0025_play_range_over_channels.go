// https://gobyexample.com/range-over-channels

package main

import "fmt"

func main() {
	queue := make(chan string, 2)
	queue <- "one"
	queue <- "two"
	close(queue) // if 'close' is removed, program is stuck!

	for elem := range queue {
		fmt.Println(elem)
	}
}
