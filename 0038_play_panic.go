// https://gobyexample.com/panic

package main

import "os"

func main() {
	_, e := os.Create("/trash.txt")
	if e != nil {
		panic(e)
	}
}
