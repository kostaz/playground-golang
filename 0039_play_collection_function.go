// https://gobyexample.com/collection-functions

package main

import (
	"fmt"
	"strings"
)

func Index(vs []string, t string) int {
	for i, v := range vs {
		if v == t {
			return i
		}
	}
	return -1
}

func Include(vs []string, t string) bool {
	return Index(vs, t) >= 0
}

func Any(vs []string, f func(string) bool) bool {
	for _, v := range vs {
		if f(v) {
			return true
		}
	}
	return false
}

func All(vs []string, f func(string) bool) bool {
	for _, v := range vs {
		if !f(v) {
			return false
		}
	}
	return true
}

func Filter(vs []string, f func(string) bool) []string {
	var vf []string
	for _, v := range vs {
		if f(v) {
			vf = append(vf, v)
		}
	}
	return vf
}

func Map(vs []string, f func(string) string) []string {
	var vm []string = make([]string, len(vs))
	for i, v := range vs {
		vm[i] = f(v)
	}
	return vm
}

func main() {
	vs := []string{"peach", "apple", "pear", "plum"}

	fmt.Println(Index(vs, "pear"))
	fmt.Println(Include(vs, "apple"))
	fmt.Println(Include(vs, "melon"))
	fmt.Println(Any(vs, func(s string) bool {
		return strings.HasPrefix(s, "p")
	}))
	fmt.Println(All(vs, func(s string) bool {
		return strings.Contains(s, "p")
	}))
	fmt.Println(Filter(vs, func(s string) bool {
		return strings.Contains(s, "a")
	}))
	fmt.Println(Map(vs, strings.ToUpper))
}
