// https://gobyexample.com/rate-limiting

// Rate limiting is an important mechanism for controlling resource utilization
// and maintaining quality of service. Go elegantly supports rate limiting with
// goroutines, channels, and tickers.

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// Prepare jobs.
	jobs := make(chan int, 10)

	jobs <- 1
	jobs <- 2
	jobs <- 3
	jobs <- 4
	jobs <- 5

	go func() {
		for {
			<-time.Tick(time.Millisecond * 100)
			jobs <- rand.Intn(100)
		}
	}()

	// Prepare rate limiter.
	permits := make(chan bool, 5) // burst up to five jobs
	for i := 0; i < 5; i++ {
		permits <- true
	}

	go func() {
		for {
			<-time.Tick(time.Millisecond * 500)
			permits <- true
		}
	}()

	for {
		<-permits
		fmt.Printf("job %03d, time %0v\n", <-jobs, time.Now())
	}
}
