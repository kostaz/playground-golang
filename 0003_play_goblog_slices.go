// https://blog.golang.org/go-slices-usage-and-internals
// Go's slice type provides a convenient and efficient means of working
// with sequences of typed data. Slices are analogous to arrays in other
// languages, but have some unusual properties. This article will look at
// what slices are and how they are used.

package main

import "fmt"

func main() {
	// An array literal can be specified like so:
	b := [2]string{"Penn", "Teller"}
	fmt.Println("b:", b)
	// Or, you can have the compiler count the array elements for you:
	c := [...]string{"Penn", "Teller"}
	fmt.Println("c:", c)
	// In both cases, the type of b is [2]string.

	// A slice literal is declared just like an array literal,
	// except you leave out the element count:
	letters := []string{"a", "b", "c", "d"}
	fmt.Println("letters:", letters)

	var s []byte
	fmt.Println("s:", s) // s: []
	s = make([]byte, 5, 1024)
	fmt.Println("s:", s) // s: [0 0 0 0 0]

	// The length and capacity of a slice can be inspected using the
	// built-in len and cap functions.
	fmt.Println("len:", len(s)) // len: 5
	fmt.Println("cap:", cap(s)) // cap: 1024

	// The zero value of a slice is nil. The len and cap functions
	// will both return 0 for a nil slice.
}
