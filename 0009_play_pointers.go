// https://gobyexample.com/pointers
// Go supports pointers, allowing you to pass references to values and records
// within your program.

package main

import "fmt"

func zeroval(n int) {
	n = 0
}

func zeroptr(n *int) {
	*n = 0
}

func main() {
	k := 777

	zeroval(k)
	fmt.Println(k)

	zeroptr(&k)
	fmt.Println(k)

	fmt.Println(&k) // 0xc42000e2f8
}
